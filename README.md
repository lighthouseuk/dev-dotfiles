# LH Dev Dotfiles

This is a collection of useful dotfiles to help with development when using bash (e.g. Terminal on Mac OS X, git Bash on Windows). It includes aliases for common commands and tools for customising the bash environment itself. 

## Modifications

The master branch contains generic code that applies to all users. When you need to make or add changes that apply only to you, branch from master and commit your changes there. That way you can merge any future updates to the master branch while retaining your modifications.

## Installation

### Using Git and the bootstrap script

You can clone the repository wherever you want. (I cloned it to `~/dev-dotfiles`) The bootstrapper script will pull in the latest version and copy the files to your home folder. **Make sure you are on the correct git branch before running bootstrap.sh**

```bash
git clone https://git@bitbucket.org:lighthouseuk/dev-dotfiles.git && cd dev-dotfiles && source bootstrap.sh
```

To update, `cd` into your local `dev-dotfiles` repository and then:

```bash
source bootstrap.sh
```

Alternatively, to update while avoiding the confirmation prompt:

```bash
set -- -f; source bootstrap.sh
```

### Specify the `$PATH`

If `~/.path` exists, it will be sourced along with the other files, before any feature testing (such as [detecting which version of `ls` is being used](https://github.com/mathiasbynens/dotfiles/blob/aff769fd75225d8f2e481185a71d5e05b76002dc/.aliases#L21-26)) takes place.

Here’s an example `~/.path` file that adds `~/utils` to the `$PATH`:

```bash
export PATH="$HOME/utils:$PATH"
```

### Add custom commands without creating a new fork

If `~/.extra` exists, it will be sourced along with the other files. You can use this to add a few custom commands without the need to fork this entire repository, or to add commands you don’t want to commit to a public repository.

Example `~/.extra`:

```bash
# Git credentials
GIT_AUTHOR_NAME="John Doe"
GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"
git config --global user.name "$GIT_AUTHOR_NAME"
GIT_AUTHOR_EMAIL="john@doe.com"
GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"
git config --global user.email "$GIT_AUTHOR_EMAIL"
```

### Sensible OS X defaults

When setting up a new Mac, you may want to set some sensible OS X defaults:

```bash
./.osx
```

### Install Homebrew formulae

When setting up a new Mac, you may want to install some common [Homebrew](http://brew.sh/) formulae (after installing Homebrew, of course):

```bash
brew bundle ~/Brewfile
```

### Install native apps with `brew cask`

You could also install native apps with [`brew cask`](https://github.com/phinze/homebrew-cask):

```bash
brew bundle ~/Caskfile
```

## Feedback

Suggestions/improvements
[welcome](https://bitbucket.org/lighthouseuk/dev-dotfiles/issues)!

## Original Author

| [Mathias Bynens](https://mathiasbynens.be/) |